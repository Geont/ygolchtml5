function Player(aName) {
        this.name = aName;
        this.lifepoints = 8000;
        this.wins = 0;
        
        this.changeLife = function(life){
            this.lifepoints = eval(this.lifepoints + '+' + life);
        };
        this.addWin = function(){
            this.wins += 1;
        };
    };
        
function LifeCounter() {
        this.playerOne = new Player('Player1');
        this.playerTwo = new Player('Player2');
        this.currentPlayer = this.playerOne;
        this.duelStarted = false;
        this.add = true;
        
        this.resetDuel = function (){
            this.playerOne.lifepoints = 8000;
            this.playerTwo.lifepoints = 8000;
        };
        
        this.checkWin = function(){
            if(this.playerOne.lifepoints <= 0){
                alert('Player 2 wins');
                this.resetDuel();
            }
            if(this.playerTwo.lifepoints <= 0){
                alert('Player 1 wins');
                this.resetDuel();
            }
        };
        
        this.changePlayer = function (){
            if (this.currentPlayer === this.playerOne){
                this.currentPlayer = this.playerTwo;
            }else{
                this.currentPlayer = this.playerOne;
            }
        };
        
        this.changeLife = function(life){
            if(this.add){
                this.currentPlayer.changeLife(life);
            }else{
                this.currentPlayer.changeLife(-life);
            }
            this.checkWin();
        };
        this.setMinus = function(){
            this.add = false;
        };
        this.setPlus = function(){
            this.add = true;
        };
        this.startDuel = function(){
            this.duelStarted = true;
        };
        
        this.save = function(){
            localStorage.setItem("player1life", this.playerOne.lifepoints);
            localStorage.setItem("player2life", this.playerTwo.lifepoints);
            localStorage.setItem("player1wins", this.playerOne.wins);
            localStorage.setItem("player2wins", this.playerTwo.wins);
            if(this.currentPlayer === this.playerOne){
                localStorage.setItem("currentPlayer", 1);
            }else{
                localStorage.setItem("currentPlayer", 2);
            }
        };
        
        this.load = function(){
            if($.isNumeric(localStorage.getItem("player1life"))){
                this.playerOne.lifepoints = localStorage.getItem("player1life");
            }
            if($.isNumeric(localStorage.getItem("player1wins"))){
                this.playerOne.wins = localStorage.getItem("player1wins");
            }
            if($.isNumeric(localStorage.getItem("player2life"))){
            this.playerTwo.lifepoints = localStorage.getItem("player2life");
        }   
            if($.isNumeric(localStorage.getItem("player2wins"))){
            this.playerTwo.wins = localStorage.getItem("player2wins");
        }
            switch(localStorage.getItem("currentPlayer")){
                case 1:
                    this.currentPlayer = this.playerOne;
                    break;
                case 2:
                    this.currentPlayer = this.playerTwo;
                    break;
            }
        };
    };
