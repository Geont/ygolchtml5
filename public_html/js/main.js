var lifeCounter = new LifeCounter();
lifeCounter.load();

function updateUI(){
    $("#player1life").text(lifeCounter.playerOne.lifepoints);
    $("#player2life").text(lifeCounter.playerTwo.lifepoints);
    lifeCounter.save();
};

updateUI();

$(".btn-number").click(function(){
    lifeCounter.changeLife($(this).text());
    updateUI();
});

$("#end-turn").click(function(){
    lifeCounter.changePlayer();
});

$("#btn-minus").click(function(){
    lifeCounter.setMinus();
});
$("#btn-plus").click(function(){
    lifeCounter.setPlus();
});
$('#btn-reset').click(function(){
    lifeCounter.resetDuel();
    updateUI();
});
$('#btn-toss-coin').click(function(){
    if(Math.floor(Math.random()*2) === 0){
        $('#coin-toss').text('Tail');
    }else{
        $('#coin-toss').text('Head');
    }
});
$('#btn-toss-dice').click(function(){
    var toss = Math.floor((Math.random()*6) + 1);
    $('#dice-toss').text(toss);
});

